﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.MainComponent.Contract;
using Lab6.Display.Contract ;

namespace Lab6.MainComponent.Implementation
{
    public class Mikrofalowka:IMikrofalowka
    {
        private bool drzwiczki;
        private bool wlaczona;

        IDisplay display;
        public Mikrofalowka(IDisplay wysw)
        {
            this.display = wysw;
        }
        public Mikrofalowka()
        {

        }
        public string Uruchom()
        {
            wlaczona = true;
            return "Kuchenka wlaczona";
        }

        public string Gotuj(string program)
        {
            if (drzwiczki && wlaczona)
                return "Gotowanie uruchomione w trybie: " + program;
            else if (wlaczona && !drzwiczki)
                return "Żeby coś ugotować trzeba zamknać drzwiczki!";
            else if (drzwiczki && !wlaczona)
            {
                return "Uruchom zanim coś zrobisz!";
            }
            else return "Błąd!";

            
        }

        public string ZamknijDrzwiczki()
        {
            drzwiczki = true;
            return "Drzwiczki zamknięte";
        }

        public string Wylacz()
        {
            return "Kuchenka wylaczona";
        }
        public void Pokaz(string text)
        {
            display.Text = text;
        }

    }
}
