﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.MainComponent.Contract
{
    public interface IMikrofalowka
    {
        string Uruchom();
        string Gotuj(string program);
        string ZamknijDrzwiczki();
        string Wylacz();
        void Pokaz(string s);
    }
}
