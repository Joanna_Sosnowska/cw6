﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.MainComponent.Contract;
using Lab6.ControlPanel.Contract;
using System.Windows;

namespace Lab6.ControlPanel.Implementation
{
    public class Adapter:IControlPanel
    {
        Window window;
        public Adapter(IMikrofalowka mikrofala)
        {
            this.window = new DisplayForm(mikrofala);
        }
        public Window Window
        {
            get { return window; }
        }
    }
}
