﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab6.MainComponent.Contract;
using Lab6.ControlPanel.Contract;
using System.Reflection;


namespace Lab6.ControlPanel.Implementation
{
    /// <summary>
    /// Interaction logic for Okno.xaml
    /// </summary>
    internal partial class DisplayForm : Window
    {
        private IMikrofalowka mikrofalowka;
        public DisplayForm(IMikrofalowka mikrofala)
        {
            var test = Assembly.GetAssembly(typeof(IMikrofalowka)).GetName().Version.Major;
            //if (Assembly.GetAssembly(typeof(IMikrofalowka)).GetName().Version.Major < 2)
            //{
               // Programy.Visibility = Visibility.Hidden;
               // Programy.Visibility = Visibility.Hidden;
           // }
            this.mikrofalowka = mikrofala;
            InitializeComponent();
        }

        private void Uruchom_Click(object sender, RoutedEventArgs e)
        {
           string action= mikrofalowka.Uruchom();
           mikrofalowka.Pokaz(action);


        }

        private void Drzwiczki_Click(object sender, RoutedEventArgs e)
        {
            string action = mikrofalowka.ZamknijDrzwiczki();
            mikrofalowka.Pokaz(action);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string action = mikrofalowka.Gotuj(Programy.SelectedIndex.ToString());
            mikrofalowka.Pokaz(action);
        }

        private void Wylacz_Click(object sender, RoutedEventArgs e)
        {
            string action = mikrofalowka.Wylacz();
            mikrofalowka.Pokaz(action);
        }
    }
}
